#!/bin/bash

docker run --name centos7 -d centos:7 sleep 600000
docker run --name ubuntu -d ubuntu:latest sleep 600000
docker run --name fedora-local -d fedora:latest sleep 600000

docker exec ubuntu /bin/bash -c "apt update; apt upgrade -y; apt install -y python3"

ansible-playbook -i inventory/prod.yml --ask-vault-pass site.yml

docker rm --force centos7
docker rm --force ubuntu
docker rm --force fedora-local